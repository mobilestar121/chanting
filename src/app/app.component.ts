import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ChantingServiceProvider } from '../providers/chanting-service/chanting-service'
import { DataProvider } from '../providers/data/data'
import { SmartAudioProvider } from '../providers/smart-audio/smart-audio';

import { IntroPage } from '../pages/intro/intro';
@Component({
  templateUrl: 'app.html',
  providers: [ChantingServiceProvider, DataProvider]
})
export class MyApp {
  rootPage:any = IntroPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, smartAudio: SmartAudioProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      smartAudio.preload('alarm', 'assets/audio/alarm.mp3');
      smartAudio.preload('click', 'assets/audio/click.mp3');
      smartAudio.preload('meditation', 'assets/audio/meditation.mp3');
    });
  }
}
