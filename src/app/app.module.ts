import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { EmailComposer } from '@ionic-native/email-composer';
import { BackgroundMode } from '@ionic-native/background-mode';

import { MyApp } from './app.component';
import { IntroPage } from '../pages/intro/intro';
import { HomePage } from '../pages/home/home';
import { NewItemPage } from '../pages/new-item/new-item';
import { PlayPage } from '../pages/play/play';
import { SettingPage } from '../pages/setting/setting';
import { InfoPage } from '../pages/info/info';

import { ListPage } from '../pages/list/list';
import { ListDetailPage } from '../pages/list-detail/list-detail';
import { TimeModalPage } from '../pages/time-modal/time-modal';
import { TimeCountPage } from '../pages/time-count/time-count';
import { ChartPage } from '../pages/chart/chart';
import { SendMailPage } from '../pages/send-mail/send-mail';
import { ProgressBarComponent } from '../components/progress-bar/progress-bar';

import { IonicAudioModule, AudioProvider, WebAudioProvider, defaultAudioProviderFactory } from 'ionic-audio';
import { ChantingServiceProvider } from '../providers/chanting-service/chanting-service';

import { DataProvider } from '../providers/data/data';
import {RoundProgressModule} from 'angular-svg-round-progressbar';

import { NativeAudio } from '@ionic-native/native-audio';
import { SmartAudioProvider } from '../providers/smart-audio/smart-audio';
import { TimerComponent } from '../components/timer/timer';
import { MultiPickerModule } from 'ion-multi-picker';

import {ChartsModule} from 'ng2-charts/charts/charts';
// import '../../node_modules/chart.js/dist/Chart.bundle.min.js';

/**
 * Sample custom factory function to use with ionic-audio
 */
export function myCustomAudioProviderFactory() {
  return new WebAudioProvider();
}

@NgModule({
  declarations: [
    MyApp,
    IntroPage,
    HomePage,
    NewItemPage,
    PlayPage,
    ListPage,
    ListDetailPage,
    TimeModalPage,
    TimeCountPage,
    SettingPage,
    InfoPage,
    ChartPage,
    SendMailPage,
    ProgressBarComponent,
    TimerComponent
  ],
  imports: [
    HttpModule,
    BrowserModule,
    ChartsModule,
    IonicModule.forRoot(MyApp),
    // IonicAudioModule.forRoot({ provide: AudioProvider, useFactory: defaultAudioProviderFactory }),
    IonicAudioModule.forRoot(defaultAudioProviderFactory),
    IonicStorageModule.forRoot(),
    RoundProgressModule,
    MultiPickerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    IntroPage,
    HomePage,
    NewItemPage,
    PlayPage,
    ListPage,
    ListDetailPage,
    TimeModalPage,
    TimeCountPage,
    SettingPage,
    InfoPage,
    ChartPage,
    SendMailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ChantingServiceProvider,
    DataProvider,
    LocalNotifications,
    NativeAudio,
    SmartAudioProvider,
    EmailComposer,
    BackgroundMode
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule {}
