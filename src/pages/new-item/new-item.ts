import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { ChantingServiceProvider } from '../../providers/chanting-service/chanting-service';

/**
 * Generated class for the NewItemPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-new-item',
  templateUrl: 'new-item.html',
})
export class NewItemPage {
  items = [];
  daily_count: number = 0;
  target_count: number = 0;
  history_count: number = 0;
  isCategorySelected = false;
  isAppCategorySelected = false;

  isSubCategorySelected = false;
  isItemSelected = false;

  isAppCategory = false;
  
  startDate: String = new Date().toISOString();
  endDate: String = new Date().toISOString();
  today = new Date().toISOString();

  // selectedValue: number;
  // categoryList: Array<{ value: number, text: string, checked: boolean }> = [];
  categoryList: any[] = [];
  subCategoryList: any[] = [];
  itemsList: any[] = [];

  appCategories: string[] = [];
  module: string;
  moduleType: string;

  categoryId: any;
  appCategoryId: any;
  subCategoryId: any;
  itemId: any;
  item_name: any;

  categoryNamesList: any[] = [];


  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private chantingService: ChantingServiceProvider, 
              public dataService: DataProvider,
              public loading: LoadingController,
              public toastCtrl: ToastController,
              public alertCtrl: AlertController) {
    this.module = "server";
    this.moduleType = "server";

    this.appCategories = ["咒語", "名號", "誦經", "拜懺", "禪定", "運動", "其它"];
    this.dataService.getData().then((data) => {
      this.items = JSON.parse(data) || [];
      console.log(this.items);
    });
    this.dataService.getCategoriesList().then((list) => {
      this.categoryNamesList = JSON.parse(list) || [];      
    })
       
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewItemPage');
    this.isCategorySelected = false;
    this.isAppCategorySelected = false;
    this.isSubCategorySelected = false;
    this.isItemSelected = false;

    this.getCategories();
  }

  switchModule(module) {
    this.moduleType = module;
    if (module === 'server') {
      // this.sortByTitle(this.tmpData);
      this.isAppCategory = false;
      this.isCategorySelected = false;
      this.categoryId = undefined;
      this.subCategoryId = undefined;
      this.isSubCategorySelected = false;      
      this.isItemSelected = false;
      this.itemId = undefined;
    }
    if (module === 'app') {
      // this.sortByDate(this.tmpData);
      this.isAppCategory = true;
      this.isAppCategorySelected = false;
      this.appCategoryId = undefined;
      this.item_name = undefined;
    }
  }  

  close() {    
    this.navCtrl.pop();
  }

  getCategories() {
    console.log("getCategories");
    let loader = this.loading.create({
      content: '',
    });
    loader.present().then(() => {
      this.chantingService.getCategories().subscribe(response => {
        loader.dismiss();   
        this.categoryList = [];
        this.categoryList = response.data;           
      }, error => {
          console.log(error);
          loader.dismiss();
          this.presentToast(error);
        });      
    });
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

  getSubCategories(subCategoryId) {
    let loader = this.loading.create({
      content: '',
    });
    loader.present().then(() => {
      this.chantingService.getSubCategories(subCategoryId).subscribe(response => {
        loader.dismiss();
        this.subCategoryList = [];
        this.subCategoryList = response.data;

        this.isSubCategorySelected = false;
        this.subCategoryId = -1;
        this.isItemSelected = false;
        this.itemId = -1;
        console.log(this.subCategoryId + "=============");
      }, error => {
          console.log(error);
          loader.dismiss();
          this.presentToast(error);
        });      
    });
  }
  getItemsInCategory(categoryId) {
    let loader = this.loading.create({
      content: '',
    });
    loader.present().then(() => {
      this.chantingService.getItemsInCategory(categoryId).subscribe(response => {
        console.log(response.data);
        loader.dismiss();
        this.itemsList = [];
        this.itemsList = response.data;

        this.isItemSelected = false;
        this.itemId = -1;

        this.target_count = response.hasNext;
        this.daily_count = this.target_count;
      }, error => {
          console.log(error);
          loader.dismiss();
          this.presentToast(error);
        });      
    });
  }

  changeHistoryCount(ev) {
    this.history_count = ev.target.value;
    var diff = this.getDatesDiff(this.startDate, this.endDate) + 1;
    // this.daily_count = Math.ceil((this.target_count-this.history_count)/diff);
    this.daily_count = Math.ceil(this.target_count/diff);
  }

  onSelectItemClicked() {
    if(!this.isCategorySelected) {
      console.log("onSelectItemClicked");
      this.showAlert('請選擇分類欄目');
      return;
    }
    if(!this.isSubCategorySelected) {
      console.log("onSelectItemClicked");
      this.showAlert('請選擇子分類欄目!');
      return;
    }
    
  }
  showAlert(message: string) {
    let alert = this.alertCtrl.create({
      title: '',
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

  categorySelected(index: number) {    
    this.isCategorySelected = true;
    var categoryId = this.categoryList[index].ID;

    if(this.categoryList[index].category_type == 'app_category') {
      this.isAppCategory = true;
    }
    if(this.categoryList[index].category_type == 'module_category') {
      this.isAppCategory = false;
      
      this.getSubCategories(categoryId);      
    }
    
  }
  appCategorySelected(index: number) {
    this.isAppCategory = true;
    this.isAppCategorySelected = true;

  }
  subCategorySelected(index: number) {
    console.log("INDEX", index);
    if(index == -1) {
      return;
    }
    this.isSubCategorySelected = true;
    
    var categoryId = this.subCategoryList[index].ID;
    console.log("subCategory", index, categoryId);  
    
    this.getItemsInCategory(categoryId);
  }
  

  itemSelected(index: number) {
    if(index == -1) {
      return;
    }
    console.log("itemSelected");
    this.isItemSelected = true;
    // this.target_count = this.itemsList[index].total;    
  }

  changeEndDateValue() {
    var diff = this.getDatesDiff(this.startDate, this.endDate) + 1;
    // this.daily_count = Math.ceil((this.target_count-this.history_count)/diff);
    this.daily_count = Math.ceil(this.target_count/diff);
  }

  getDatesDiff(startDate, endDate) {
    var startTime = new Date(startDate);  // Feb 1, 2011
    var endTime = new Date(endDate);              // now
    var diff = endTime.getTime() - startTime.getTime();   // now - Feb 1
    diff = diff / (1000*60*60*24); // positive number of days
    return diff;
  }

  save(categoryId, appCategoryId, itemId, item_name, target_count, startDate, endDate, daily_count, history_count) {
    console.log('new item added ' + categoryId + ' ' + itemId + ' ' + target_count  + ' ' + startDate  + ' '+endDate  + ' ' + daily_count);
    
    var created = new Date().toISOString();

    if(target_count === 0) {
      this.showAlert('目標遍數不能是零');
      return;
    }
    if(this.moduleType === 'app') {
      if(appCategoryId == undefined) {
        this.showAlert('請選擇分類欄目');
        return;
      }
      if(item_name == '' || item_name==undefined) {
        this.showAlert('請輸入功課名稱!');
        return;
      }

      let categoryName = this.appCategories[appCategoryId];
      var item = {
          categoryId: appCategoryId,
          categoryName: categoryName,
          category_type: "app_category",  // {module_category, app_category}
          item: {
            ID: "",
            item_name: item_name,
            item_description: "",
            audio_name: "",
            item_image: ""
          },
          target_count: target_count,
          startDate: startDate,
          endDate: endDate,
          daily_count: daily_count,
          complete: 0,
          history_count: history_count,
          created:created,
          updated:created
        }
      this.items.push(item);
      this.dataService.save(this.items);
      this.saveCategoryToNamesList(categoryName);
      this.navCtrl.popToRoot();
    }
    if(this.moduleType === 'server') {
      if(categoryId == undefined) {
        this.showAlert('請選擇分類欄目');
        return;
      }
      if(this.subCategoryId == undefined || this.subCategoryId == -1) {
        this.showAlert('請選擇子分類欄目');
        return;
      }
      if(this.itemId == undefined || this.itemId == -1) {
        this.showAlert('請選擇一項功課');
        return;
      }

      let categoryName = this.categoryList[categoryId].category_name;
      item = {
          categoryId: this.categoryList[categoryId].ID,
          categoryName: categoryName,
          category_type: this.categoryList[categoryId].category_type,  // {module_category, app_category}
          item: this.itemsList[itemId],
          target_count: target_count,
          startDate: startDate,
          endDate: endDate,
          daily_count: daily_count,
          complete: 0,
          history_count: history_count,
          created:created,
          updated:created
        }
      this.items.push(item);
      this.dataService.save(this.items);
      this.saveCategoryToNamesList(categoryName);

      this.navCtrl.popToRoot();
    }   
  }
  existNameInCategories(categoryName) {
    if(this.categoryNamesList.indexOf(categoryName) > -1)
      return true
    return false;
  }
  saveCategoryToNamesList(categoryName) {
    if(!this.existNameInCategories(categoryName)) {
        this.categoryNamesList.push(categoryName);
        this.dataService.setCategoriesList(this.categoryNamesList);
      }
  }

}
