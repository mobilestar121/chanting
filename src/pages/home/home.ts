import { Component } from '@angular/core';
import { NavController, AlertController, Events } from 'ionic-angular';
import { PlayPage } from '../play/play';
import { SettingPage } from '../setting/setting';
import { TimeModalPage } from '../time-modal/time-modal';
import { InfoPage } from '../info/info';

import { ListPage } from '../list/list';

import { ChartPage } from '../chart/chart';
import { NativeAudio } from '@ionic-native/native-audio';

import { DataProvider } from '../../providers/data/data';
import { SmartAudioProvider } from '../../providers/smart-audio/smart-audio';
import moment from 'moment';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  data: any[];

  loadProgress = 0;
  total = 0;
  badgeCount: number = 0;
  category: any;
  // itemsList: any[] = [];
  isItemSelected = false;

  noMusic = true;
  musicItem: any;

  isToggled = false;
  isStart = false;

  constructor(public navCtrl: NavController,
              public dataService: DataProvider,
              public alertCtrl: AlertController,
              public nativeAudio: NativeAudio,
              public events: Events,
              public smartAudio: SmartAudioProvider) {
    this.data = [];

    // set a key/value

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
    this.dataService.getButtonClickSoundOnOffStatus().then((status) => {
      if(status == null) {
        this.dataService.setButtonClickSoundOnOffStatus(true);
        this.isToggled = true;
      } else {
        this.isToggled = status;
      }
      console.log("status", this.isToggled);
    });

    this.dataService.getAlarmStatus().then((status) => {
        if(status == null) {
          this.dataService.setAlarmStatus(true);          
        }
      });

  }
  ionViewWillEnter() {
    this.dataService.getData().then((data) => {
      if(data){
        this.data = JSON.parse(data) || [];
        console.log("Home Data", this.data);
      }
    });

    this.events.subscribe('reloadHomePage',() => {
      console.log("reloadHomePage");
      // this.navCtrl.pop();
      // this.navCtrl.push(HomePage);
    });
  }

  onSelectItemClicked() {
    this.showAlert('請先建立新功課');
  }
  showAlert(subTitle: string) {
    let alert = this.alertCtrl.create({
      title: '',
      subTitle: subTitle,
      buttons: ['OK']
    });
    alert.present();
  }

  leftPad(number, targetLength) {
    var output = number + '';
    while (output.length < targetLength) {
        output = '0' + output;
    }
    return output;
  }

  changeCategory(index: number) {
    console.log("index" + index);
    this.isItemSelected = true;
    this.category = index;
    this.dataService.getData().then((data) => {
      if(data){
        this.data = JSON.parse(data) || [];

        var selectedItem = this.data[index];
        this.total = selectedItem.target_count;
        this.loadProgress = selectedItem.complete;

        // this.badgeCount = Math.floor(this.loadProgress / this.total);
        this.badgeCount = selectedItem.complete + parseInt(selectedItem.history_count);

        if (selectedItem.category_type == "module_category") {
          console.log("changeCategory "+this.loadProgress + " " + this.total);
          if(selectedItem.item.audio_name) {            
            this.noMusic = false;
          } else {
            console.log("item", selectedItem);
            console.log("audio item", selectedItem.item.audio_name);
            this.noMusic = true;
          }
          
          this.musicItem = selectedItem;
        }
        if (selectedItem.category_type == "app_category") {
          this.noMusic = true;
          this.musicItem = null;
        }

      }
    });
  }

  itemSelected(item) {
    // alert(item.name);
    this.navCtrl.push(ChartPage, {
      item: item
    });
  }

  onAddNewItem() {
    this.navCtrl.push(ListPage);
    // this.navCtrl.push(NewItemPage);
  }
  onPlusClicked() {    
    console.log("isToggled", this.isToggled);
    this.dataService.getAlarmStatus().then((status) => {
      if(status) {
        if(this.isToggled) {
          this.smartAudio.play('click');
        }
      }
    });
    

    if(this.category == undefined) {
      this.showAlert('請選擇一項功課');
      return;
    }
    this.loadProgress++;
    if(this.loadProgress%this.total == 0) {
      this.isStart = false;
      this.dataService.getAlarmStatus().then((status) => {
        if(status) {
          this.playSound();
        }
      });
    }
    if(this.loadProgress%this.total == 1 && this.loadProgress/this.total>=1 && !this.isStart) {
      this.loadProgress--;
      this.isStart = true;
      return;
    }
    var updated = new Date().toISOString();

    var selectedItem = this.data[this.category];
    selectedItem.target_count = this.total;
    selectedItem.complete = this.loadProgress;
    selectedItem.updated = updated;

    // read Items Array in detail Item of Chant list
    let readItems : any[] = selectedItem.readItems || [];
    if(readItems.length == 0) {
      let readItem = {readDate : updated, 
                      readCount : 1};
      readItems.push(readItem);
      selectedItem.readItems = readItems;
    } else {
      let lastReadItem = readItems[readItems.length-1]

      var lastReadDate = moment(new Date(lastReadItem.readDate));
      var currentDate = moment(new Date());
      
      // if same date
      if(lastReadDate.isSame(currentDate, 'year') 
              && lastReadDate.isSame(currentDate, 'month') 
              && lastReadDate.isSame(currentDate, 'day')) {// [days, years, months, seconds, ...]
        readItems[readItems.length-1].readDate = updated;
        readItems[readItems.length-1].readCount = readItems[readItems.length-1].readCount + 1;

        selectedItem.readItems = readItems;
      } else {
        let readItem = {readDate : updated, 
                      readCount : 1};
        readItems.push(readItem);
        selectedItem.readItems = readItems;
      }
    }

    // this.badgeCount = Math.floor(this.loadProgress / this.total);
    this.badgeCount = this.loadProgress + parseInt(selectedItem.history_count);

    this.data[this.category] = selectedItem;
    console.log("Plus Clicked-data "+this.data);
    this.dataService.save(this.data);
  }

  isComplete() {
    return (this.loadProgress%this.total == 0) 
          && (this.loadProgress/this.total >= 1)
          && !this.isStart
  }

  playSound() {
    this.smartAudio.play('alarm');
  }

  onNotifyAlam() {
    this.dataService.setButtonClickSoundOnOffStatus(!this.isToggled);
    this.dataService.getButtonClickSoundOnOffStatus().then((status) => {
      this.isToggled = status || false;
      console.log("status", this.isToggled);
    });
  }
  onRefresh() {
    var selectedItem = this.data[this.category];
    if(selectedItem == undefined)
      return;

    let readItems : any[] = selectedItem.readItems || [];
    if(readItems.length == 0)
      return;
    var offset = this.loadProgress%this.total;
    // this.loadProgress = this.badgeCount * this.total;
    this.loadProgress = this.loadProgress - offset;

    let readCountSum = 0;
    let count = 0;
    for(let i = readItems.length-1; i>=0 ; i--) {
      readCountSum += readItems[i].readCount;
      count++;
      if(readCountSum >= offset) {
        readItems.splice(readItems.length-1-count, count);
        selectedItem.readItems = readItems;
        break;
      }
      // if(readCountSum > offset) {
      //   readItems.splice(readItems.length-count, count-1);
      //   let diff = readCountSum - offset;
      //   readItems[readItems.length-1-count].readCount = readItems[readItems.length-1-count].readCount - diff;

      //   selectedItem.readItems = readItems;
      //   break;
      // }
    }

    selectedItem.complete = this.loadProgress;

    this.badgeCount = this.loadProgress + parseInt(selectedItem.history_count);

    console.log("Complete", selectedItem.complete);
    this.data[this.category] = selectedItem;
    this.dataService.save(this.data);
  }
  public openPlayPage(): void {
      this.navCtrl.push(PlayPage, {
        musicItem: this.musicItem.item
      });
  }

  public openMediationPage() {
    this.navCtrl.push(TimeModalPage);
  }

  public openSettingPage(): void {
    this.navCtrl.push(SettingPage);
  }

  public infoPage(): void {
    this.navCtrl.push(InfoPage);
  }



  public openListPage(): void {
    this.navCtrl.push(ListPage, {
      item: {name: "Example", id: 1}
    });
  }

}
