import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { TimeModalPage } from '../time-modal/time-modal';

/**
 * Generated class for the SettingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {

  public isToggled: boolean;

  constructor(public navCtrl: NavController,
              public modalCtrl: ModalController,
              public dataService: DataProvider,
              public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingPage');
    this.dataService.getAlarmStatus().then((status) => {
      this.isToggled = status || false;
      console.log("status", this.isToggled);
    });
  }
  
  close() {
    this.navCtrl.pop();
  }

  public notify() {
    this.dataService.setAlarmStatus(this.isToggled);
  }

  onClickAlarm() {
    this.presentTimeModal();
  }

  presentTimeModal() {
    // let profileModal = this.modalCtrl.create(TimeModalPage, { userId: 8675309 });
    // profileModal.present();
    this.navCtrl.push(TimeModalPage);
  }


}
