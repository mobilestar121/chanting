import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AudioProvider } from 'ionic-audio';

/**
 * Generated class for the PlayPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-play',
  templateUrl: 'play.html',
})
export class PlayPage {
  musicItem: any;

  myTracks: any[];
  singleTrack: any;
  allTracks: any[];
  currentTrack: any;

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlayPage');
  }

  close() {
    this.navCtrl.pop();
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, private _audioProvider: AudioProvider) {

    this.musicItem = navParams.get('musicItem');

    console.log("audio link", this.musicItem.audio_name);

    // plugin won't preload data by default, unless preload property is defined within json object - defaults to 'none'
    /*
    this.myTracks = [{
      src: 'https://archive.org/download/JM2013-10-05.flac16/V0/jm2013-10-05-t12-MP3-V0.mp3',
      artist: 'John Mayer',
      title: 'Why Georgia',
      art: 'assets/img/johnmayer.jpg',
      playing: false,
      preload: 'metadata' // tell the plugin to preload metadata such as duration for this track, set to 'none' to turn off
    },
    {
      src: 'https://archive.org/download/JM2013-10-05.flac16/V0/jm2013-10-05-t30-MP3-V0.mp3',
      artist: 'John Mayer',
      title: 'Who Says',
      art: 'assets/img/johnmayer.jpg',
      playing: false,
      preload: 'metadata' // tell the plugin to preload metadata such as duration for this track,  set to 'none' to turn off
    },
    {
      src: 'https://archive.org/download/swrembel2010-03-07.tlm170.flac16/swrembel2010-03-07s1t05.mp3',
      artist: 'Stephane Wrembel',
      title: 'Stephane Wrembel Live',
      art: 'assets/img/Stephane.jpg',
      playing: false,
      // preload: 'metadata' // tell the plugin to preload metadata such as duration for this track,  set to 'none' to turn off
    }];
    */
    this.myTracks = [];
    
    this.singleTrack = {
      src: this.musicItem.audio_name,
      artist: 'Stephane Wrembel',
      title: this.musicItem.item_name,
      art: this.musicItem.item_image,
      playing: false,
      // preload: 'metadata' // tell the plugin to preload metadata such as duration for this track,  set to 'none' to turn off
    }

    this.myTracks.push(this.singleTrack);

    this.currentTrack = this.myTracks[0];
  }

  ngAfterContentInit() {   
    
    // get all tracks managed by AudioProvider so we can control playback via the API
    this._audioProvider.tracks.length = 0;
    this.allTracks = this._audioProvider.tracks; 
    console.log("ngAfterContentInit " + this.allTracks.length);  
  }
  ngOnDestroy() {
    this.allTracks = [];
  }

  playTrack(track){
      // First stop any currently playing tracks

      for(let checkTrack of this.myTracks){
          if(checkTrack.playing){
            console.log("Check Track " + checkTrack.title);
              this.pauseTrack(checkTrack);
          }
      }

      track.playing = true;
      this.currentTrack = track;

      let index = this.myTracks.indexOf(track);
    // use AudioProvider to control selected track 
    this._audioProvider.play(index);
  }
  pauseTrack(track){
      track.playing = false;
      let index = this.myTracks.indexOf(track);
     // use AudioProvider to control selected track 
     this._audioProvider.pause(index);  
  }
         
  onTrackFinished(track: any) {
    console.log('Track finished', track)
  }

  prevPlay() {
    // console.log("Prev clicked");
    // let index = this.myTracks.indexOf(this.currentTrack);
    // index > 0 ? index-- : index = this.myTracks.length - 1;
    
    // // this.currentTrack = this.myTracks[index];
    // this.playTrack(this.myTracks[index]);
  }
  nextPlay() {
    
    // let index = this.myTracks.indexOf(this.currentTrack);
    // index >= this.myTracks.length - 1 ? index = 0 : index++;
    // console.log("Next clicked" + index);
    // // this.currentTrack = this.myTracks[index];

    // this.playTrack(this.myTracks[index]);
  }

}
