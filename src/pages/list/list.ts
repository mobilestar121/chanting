import { Component, ViewEncapsulation } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NewItemPage } from '../new-item/new-item';
import { DataProvider } from '../../providers/data/data';
import { ListDetailPage } from '../list-detail/list-detail';
import { SendMailPage } from '../send-mail/send-mail';

import { ChantingServiceProvider } from '../../providers/chanting-service/chanting-service';
import moment from 'moment';

/**
 * Generated class for the ListPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
  encapsulation: ViewEncapsulation.None
})
export class ListPage {
  item: any;
  sort: string;
  data = [];
  tmpData = [];

  selectedCategoryName: string;
  sortType: string;
    
  
  // current: number = 27;
  // max: number = 50;
  stroke: number = 4;
  radius: number = 20;
  semicircle: boolean = false;
  rounded: boolean = false;
  responsive: boolean = false;
  clockwise: boolean = true;
  color: string = '#BE8947';
  background: string = '#eaeaea';
  duration: number = 800;
  animation: string = 'easeOutCubic';
  animationDelay: number = 0;
  animations: string[] = [];
  gradient: boolean = false;
  realCurrent: number = 0;

  isCategorySelected = false;
  categoryList: any[] = [];

  categoryId: number;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public dataService: DataProvider,
              private chantingService: ChantingServiceProvider) {
    this.sort = "title";
    this.sortType = "title";
    this.item = navParams.get('item');
    
  }
  
  ionViewDidLoad() {
    this.dataService.getData().then((data) => { 
      if(data){
        this.data = JSON.parse(data) || [];
        this.tmpData = this.data;
        if(this.data.length > 0) {
          this.sortByTitle(this.tmpData);
        }
      } 
    });
    this.dataService.getCategoriesList().then((list) => {
      this.categoryList = JSON.parse(list) || [];
      this.categoryList.push("全部"); 

      this.categoryId = this.categoryList.length-1;
      this.categorySelected(this.categoryId);
    })
    // this.getCategories();
  }

  getCategories() {
    this.chantingService.getCategories().subscribe(response => {
      this.categoryList = [];
      this.categoryList = response.data;      
    })
  }

  getOverlayStyle() {
    let isSemi = this.semicircle;
    let transform = (isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

    return {
      'top': isSemi ? 'auto' : '50%',
      'bottom': isSemi ? '5%' : 'auto',
      'left': '50%',
      'transform': transform,
      '-moz-transform': transform,
      '-webkit-transform': transform,
      'font-size': this.radius / 2.5 + 'px'
    };
  }

  categorySelected(index: number) {
    this.isCategorySelected = true;
    var categoryName = this.categoryList[index];
    this.selectedCategoryName = categoryName;

    if(index < this.categoryList.length-1)
      this.filterByCategory(categoryName, this.tmpData);
    else {
      this.filterByCategory(categoryName, this.tmpData);
    }
  }
  getPropotion(complete: number, target_count: number) {
    return (complete%target_count==0&&complete>0)?"100%":Math.round((complete%target_count)/target_count *100) + "%";
  }

  getStartDate(detailItem) { // using moment.js
    var date = new Date(detailItem.created);
    var formattedDate = moment(date).format('DD/MM/YYYY h:mm a');
    return formattedDate;
  }

  close() {
    this.navCtrl.pop();
  }
  onAddItem() {
    this.navCtrl.push(NewItemPage);
  }

  onListDetailItem(detailItem) {
    this.navCtrl.push(ListDetailPage, {
      detailItem: detailItem
    });
  }

  sortList(sort) {
    this.sortType = sort;
    if (sort === 'title') {
      this.sortByTitle(this.tmpData);
    }
    if (sort === 'date') {
      this.sortByDate(this.tmpData);
    }
    if (sort === 'complete') {
      this.sortByComplete(this.tmpData);
    }
    this.selectCategoryFilter();
  }

  selectCategoryFilter() {
    if(this.selectedCategoryName != undefined) {
      this.filterByCategory(this.selectedCategoryName, this.tmpData);
    }
  }
  sortByTitle(sourceData:any[]) {
    this.data = sourceData && sourceData.map(item => {
      item.title = item.item && item.item.item_name;
      return item;
    }).sort(this.sortByProperty('title'));
  }
  sortByDate(sourceData:any[]) {
    this.data = sourceData && sourceData.sort(this.sortByProperty('startDate')).reverse();
  }
  sortByComplete(sourceData:any[]) {
    // this.data = sourceData && sourceData.sort(this.sortByProperty('complete'));
    this.data = sourceData && sourceData.filter(item => (item.complete%item.target_count == 0 && item.complete/item.target_count >= 1)).reverse();
  }

  filterByCategory(categoryName: string, sourceData:any[]) {
    if(categoryName === '全部'){
      this.data = sourceData;
    } else
      this.data = sourceData && sourceData.filter(item => item.categoryName == categoryName);
    if (this.sortType === 'title') {
      this.sortByTitle(this.data);
    }
    if (this.sortType === 'date') {
      this.sortByDate(this.data);
    }
    if (this.sortType === 'complete') {
      this.sortByComplete(this.data);
    }
  }

  private sortByProperty(property: string) {
    return (a, b) => {
      if (!a[property] || !b[property])
        return 0;
      return (a[property] < b[property]) ? -1 : 1;
    }
  }

  removeItem(item) {
    // remove item from only view
    for(let i = 0; i < this.data.length; i++) { 
      if(this.data[i] == item){
        this.data.splice(i, 1);
      } 
    }
    // remove item from storage
    for(let i = 0; i < this.tmpData.length; i++) { 
      if(this.tmpData[i] == item){
        this.tmpData.splice(i, 1);
        this.dataService.save(this.tmpData);
      }
    }
    // remove item from category list storage
    if(this.tmpData.indexOf(item.categoryName) == -1) {
      let index = this.categoryList.indexOf(item.categoryName);
      this.categoryList.splice(index, 1);

      let tmpList = [];
      for(let i=0;i<this.categoryList.length-1;i++) {
        tmpList.push(this.categoryList[i]);
      }
      this.dataService.setCategoriesList(tmpList);

      this.categoryId = this.categoryList.length-1;
      this.categorySelected(this.categoryId);
    }
    for(let i = 0; i < this.tmpData.length; i++) {
      if(this.tmpData[i].categoryName == item.categoryName){

      }
    }
  }

  openMailPage() {
    this.navCtrl.push(SendMailPage, {
      page: "complete",
      detailItem: this.data
    });
  }

}
