import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { EmailComposer } from '@ionic-native/email-composer';
import { EmailValidator } from  '../../validators/email';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import moment from 'moment';
import { ChantingServiceProvider } from '../../providers/chanting-service/chanting-service';
import { DataProvider } from '../../providers/data/data';

/**
 * Generated class for the SendMailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-send-mail',
  templateUrl: 'send-mail.html',
})
export class SendMailPage {
  pageType: any;
  detailItem: any;
  name:AbstractControl;
  registerForm: FormGroup;
  email: AbstractControl;
  
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public emailComposer: EmailComposer, 
              private chantingService: ChantingServiceProvider,
              public loading: LoadingController,
              public toastCtrl: ToastController,
              public dataService: DataProvider,
              public formBuilder: FormBuilder) {
    this.pageType = navParams.get('page');
    this.detailItem = navParams.get('detailItem');

    this.registerForm = formBuilder.group({
            'email': ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.required, Validators.maxLength(25), EmailValidator.isValidMailFormat])],
            'name': ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z]*')])]
            }
        );

    this.email = this.registerForm.controls['email'];
    this.name = this.registerForm.controls['name'];

    this.dataService.getUserData().then((data) => {
      let userData = JSON.parse(data);
      if(userData) {
        this.name.setValue(userData.userName);
        this.email.setValue(userData.userEmail);
      }      
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SendMailPage');
    
  }
  close() {
    this.navCtrl.pop();
  }
  sendMail() {
    this.dataService.setUserData({userName:this.name.value, userEmail:this.email.value});

    var body: string = '';
    if(this.pageType === 'detail') {
      body = '<h3>'+this.detailItem.item.item_name+'</h3><p></p>'
            +'<div>分類:'+this.detailItem.categoryName+'</div>'
            +'<div>名稱:'+this.detailItem.item.item_name+'</div>'
            +'<div>目標遍數:'+this.detailItem.target_count+'</div>'
            +'<div>歷史遍數:'+this.detailItem.history_count+'</div>'
            +'<div>當前遍數:'+this.detailItem.complete+'</div>'
            +'<div>總遍數:'+this.getTotalCount(this.detailItem.history_count, this.detailItem.complete)+'</div>'
            +'<div>總計完成:'+this.getCompletePercentage(this.detailItem.complete, this.detailItem.target_count)+'</div>'
            +'<div>建立日期:'+this.getStartDate(this.detailItem.created)+'</div>'
            +'<div>完成日期:'+this.getFinishDate(this.detailItem.endDate)+'</div>'
            +'<div>最後功課日期:'+this.getLastCoundDate(this.detailItem.updated)+'</div>';
    }
    if(this.pageType === 'complete') {
      console.log("detailItem", this.detailItem);
      for(var i=0; i<this.detailItem.length; i++) {
        
        let subBody = '<div>分類:'+this.detailItem[i].categoryName+'</div>'
            +'<div>名稱:'+this.detailItem[i].item.item_name+'</div>'
            +'<div>目標遍數:'+this.detailItem[i].target_count+'</div>'
            +'<div>歷史遍數:'+this.detailItem[i].history_count+'</div>'
            +'<div>當前遍數:'+this.detailItem[i].complete+'</div>'
            +'<div>總遍數:'+this.getTotalCount(this.detailItem[i].history_count, this.detailItem[i].complete)+'</div>'
            +'<div>總計完成:'+this.getCompletePercentage(this.detailItem[i].complete, this.detailItem[i].target_count)+'</div>'
            +'<div>建立日期:'+this.getStartDate(this.detailItem[i].created)+'</div>'
            +'<div>完成日期:'+this.getFinishDate(this.detailItem[i].endDate)+'</div>'
            +'<div>最後功課日期:'+this.getLastCoundDate(this.detailItem[i].updated)+'</div><br>';
            
        body += subBody;
        console.log("body", body);
      }
    }
    
    // let emailCode = {
    //   to: this.email.value,
    //   cc: '',
    //   bcc: [],
    //   attachments: [
    //   ],
    //   subject: this.name.value,
    //   body: body,
    //   isHtml: true
    // };

    // // Send a text message using default options
    // this.emailComposer.open(emailCode);
    let loader = this.loading.create({
      content: 'Sending...',
    });
    loader.present().then(() => {
      this.chantingService.getMail(this.name.value, this.email.value, body, this.pageType)
        .subscribe(data => {
          console.log("status", data["_body"]);
          console.log("data", data);
          this.presentToast('Email sent successfully');
        }, error => {
          console.log(error);
          this.presentToast(error);
        });
      loader.dismiss();
    });
        
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }
  getTotalCount(history_count, complete) {
    return history_count + parseInt(complete);
  }
  getCompletePercentage(complete, target) {
    return Math.round((complete%target)/target *100) + "%";
  }
  getStartDate(created) { // using moment.js
    var date = new Date(created);
    var formattedDate = moment(date).format('DD/MM/YYYY');
    return formattedDate;
  }
  getFinishDate(endDate) {
    var date = new Date(endDate);
    var formattedDate = moment(date).format('DD/MM/YYYY');
    return formattedDate;    
  }
  getLastCoundDate(updated) {
    var date = new Date(updated);
    var formattedDate = moment(date).format('DD/MM/YYYY');
    return formattedDate;
  }

}
