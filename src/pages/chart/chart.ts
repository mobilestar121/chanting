import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import moment from 'moment';
/**
 * Generated class for the ChartPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 * 
 * Angular Chart
 * https://www.highcharts.com/docs/chart-concepts/series
 */
@IonicPage()
@Component({
  selector: 'page-chart',
  templateUrl: 'chart.html',
})
export class ChartPage {
  @ViewChild('lineCanvas') lineCanvas;

  readonly WEEK_DAYS = 7;
  readonly MONTH_DAYS = 31;
  readonly HALF_DAYS = 12; // 15days * 2 * 6 = 6months

  detailItem: any;
  navTitle: string;

  switch: string;

  chartOptions: any = {
      scaleShowVerticalLines: true,
      responsive: true,
      scales: {
        yAxes: [{
            ticks: {
                beginAtZero: true,
            }
        }]
      }
  }; 
  
  chartType: string = 'line';
  chartLegend: boolean = true; 
  chartLabels: string[] = [];
  chartData: any[] = [];
  public chartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];


  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.detailItem = navParams.get('detailItem');
    this.navTitle = this.detailItem.item.item_name;
    this.switch = "week";    
  }

  ngOnInit() {
      this.setWeekData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChartPage');
    console.log("getLastHalfYearDays()", this.getLastHalfYearDays()); 
  }

  close() {
    this.navCtrl.pop();
  }

  getTodayCount() {
    var startDate = moment(new Date(this.detailItem.startDate));
    var endDate = moment(new Date(this.detailItem.endDate));

    return startDate.diff(endDate, 'days') + 1;
    // let today = moment(new Date());
    // if(this.detailItem.readItems) {
    //     for(let entry of this.detailItem.readItems) {
    //       let entryDate = moment(new Date(entry.readDate));
          
    //       if(today.isSame(entryDate, 'year') 
    //           && today.isSame(entryDate, 'month') 
    //           && today.isSame(entryDate, 'day')) {
    //             return entry.readCount;
    //       }
    //     }
    // }
    // return 0;
    
  }
  getDailyAverageCount() {    
    var startDate = moment(new Date(this.detailItem.startDate));
    var today = moment(new Date());

    return startDate.diff(today, 'days') + 1;

    // let daysDiff = startDate.diff(today, 'days') + 1;
    // let sum = 0;
    // if(this.detailItem.readItems) {
    //   for(let entry of this.detailItem.readItems) {
    //     sum += entry.readCount;
    //   }
    //   return Math.round(sum/daysDiff);
    // } else {
    //   return 0;
    // }
  }
  getLastHalfYearDays() {
    var daysAgo: string[] = [];
    for(var i=0; i<this.HALF_DAYS; i++) {
      // daysAgo[i] = moment().subtract(i, 'days').format("DD MM YYYY")
      daysAgo[i] = moment().subtract(i*15+1, 'days').format('DD MM')      
    }
    return daysAgo    
  }

  getLastHalfYearMomentDays() {
    var daysAgo: string[] = [];
    for(var i=0; i<this.HALF_DAYS; i++) {
      daysAgo[i] = moment().subtract(i*15+1, 'days').toDate().toISOString();     
    }
    return daysAgo    
  }

  getDaysOfBlock(day) {
    let sum = 0;
    for(var i=1;i<=15;i++) {
      let tmpDay = moment(day).subtract(i, 'days')
      for(let entry of this.detailItem.readItems) {
        let entryDate = moment(new Date(entry.readDate));
            if(tmpDay.isSame(entryDate, 'year') 
                && tmpDay.isSame(entryDate, 'month') 
                && tmpDay.isSame(entryDate, 'day')) {
              sum += entry.readCount;
            }
      }
    }
    return sum;
  }

  getDataOfLastHalfYear() {
    let daysOfBlocks = this.getLastHalfYearMomentDays();    
    var countSumOfBlocks: number[] = [];
    for(var j=0; j<this.HALF_DAYS; j++) {
      countSumOfBlocks[j] = 0;
    }
    if(this.detailItem.readItems && this.detailItem.readItems.length>0) {
      for(var i=0; i<daysOfBlocks.length; i++) {
        if(i===0) {
          let day0 = moment(daysOfBlocks[0]);

          countSumOfBlocks[i] = 0;
          for(let entry of this.detailItem.readItems) {
            let entryDate = moment(new Date(entry.readDate));
            if(day0.isSame(entryDate, 'year') 
                && day0.isSame(entryDate, 'month') 
                && day0.isSame(entryDate, 'day')) {
              countSumOfBlocks[i] = entry.readCount;
            } 
          }
        } else {
          countSumOfBlocks[i] = this.getDaysOfBlock(daysOfBlocks[i-1]);
        }
      }
    }

    return countSumOfBlocks;
    
  }

  getLastWeekDays() {
    var daysAgo: string[] = [];
    for(var i=1; i<=this.WEEK_DAYS; i++) {
      // daysAgo[i] = moment().subtract(i, 'days').format("DD MM YYYY")
      daysAgo[i-1] = moment().subtract(i, 'days').format('dddd')
    }
    return daysAgo    
  }  

  showLastWeekData() {
    var weekData: number[] = [];
    for(var i=1; i<=this.WEEK_DAYS; i++) {
      // daysAgo[i] = moment().subtract(i, 'days').format("DD MM YYYY")
      var daysAgo = moment().subtract(i, 'days')
      weekData[i-1] = 0;
      if(this.detailItem.readItems) {
        for(let entry of this.detailItem.readItems) {
          let entryDate = moment(new Date(entry.readDate));
          
          if(daysAgo.isSame(entryDate, 'year') 
              && daysAgo.isSame(entryDate, 'month') 
              && daysAgo.isSame(entryDate, 'day')) {
            weekData[i-1] = entry.readCount;
            break;
          } 
        }
      }      
    }
    return weekData;    
  }
  getLastMonthDays() {
    var daysAgo: string[] = [];
    for(var i=1; i<=this.MONTH_DAYS; i++) {
      // daysAgo[i] = moment().subtract(i, 'days').format("DD MM YYYY")
      daysAgo[i-1] = moment().subtract(i, 'days').format('MM/DD')
    }
    return daysAgo
  }

  showLastMonthData() {
    var weekData: number[] = [];
    for(var i=1; i<=this.MONTH_DAYS; i++) {
      // daysAgo[i] = moment().subtract(i, 'days').format("DD MM YYYY")
      var daysAgo = moment().subtract(i, 'days')
      weekData[i-1] = 0;
      if(this.detailItem.readItems) {
        for(let entry of this.detailItem.readItems) {
          let entryDate = moment(new Date(entry.readDate));
          
          if(daysAgo.isSame(entryDate, 'year') 
              && daysAgo.isSame(entryDate, 'month') 
              && daysAgo.isSame(entryDate, 'day')) {
            weekData[i-1] = entry.readCount;
            break;
          } 
        }
      }      
    }
    return weekData;    
  }

  // for all data
  getAllMonths() {
    var dateStart = moment(new Date(this.detailItem.startDate));
    var today = moment(new Date());
    var timeValues = [];

    while (today > dateStart) {
      timeValues.push(dateStart.format('YYYY-MM'));
      dateStart.add(1,'month');
    }
    return timeValues;
  }
  getAllData() {
    var allData: number[] = [];
    for(var i=0;i<this.getAllMonths().length;i++) {
      allData[i] = 0;
    }
    for(var i=0;i<this.getAllMonths().length;i++) {
      let dateAgo = moment(this.getAllMonths[i]);
      if(this.detailItem.readItems) {
        for(let entry of this.detailItem.readItems) {
          let entryDate = moment(new Date(entry.readDate));
          if(dateAgo.isSame(entryDate, 'year') 
              && dateAgo.isSame(entryDate, 'month') ) {
            allData[i] += entry.readCount;
          }
        }
      }
    }
    return allData;
  }

  changeType(type) {
    if (type === 'week') {
        this.setWeekData();
    }
    if (type === 'month') {        
        this.setMonthData();
    }
    if (type === 'half_year') {
        this.setHalfYearData();        
    }
    if (type === 'all') {
        this.setAllData();
    }
  }
  setWeekData(){
      this.chartLabels = this.getLastWeekDays().reverse(); //['Monday', 'TuesDay', 'Wednesday', 'Thirsday', 'Friday', 'Saturday', 'Sunday'];
      console.log("chart", this.chartLabels);
      console.log("data", this.showLastWeekData());
      this.chartData = [
            { data: this.showLastWeekData().reverse(), label: 'Count' }
        ];
  }
  setMonthData(){
    this.chartLabels = this.getLastMonthDays().reverse();
    this.chartData = [
          { data: this.showLastMonthData().reverse(), label: 'Count' }
      ];
  }
  setHalfYearData() {
    this.chartLabels = this.getLastHalfYearDays().reverse();
    this.chartData = [
          { data: this.getDataOfLastHalfYear().reverse(), label: 'Count' }
      ];
    // this.chartLabels = ['January', 'February', 'March', 'April', 'May', 'June'];
    // this.chartData = [
    //       { data: [75, 80, 45, 100, 60, 70], label: 'Count' }
    //   ];
  }
  setAllData() {
    this.chartLabels = this.getAllMonths().reverse();
    this.chartData = [
          { data: this.getAllData().reverse(), label: 'Count' }
      ];
  }

  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }
}
