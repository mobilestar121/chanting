import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TimeCountPage } from './time-count';

@NgModule({
  declarations: [
    TimeCountPage,
  ],
  imports: [
    IonicPageModule.forChild(TimeCountPage),
  ],
  exports: [
    TimeCountPage
  ]
})
export class TimeCountPageModule {}
