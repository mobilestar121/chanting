import { TimerComponent } from '../../components/timer/timer';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { BackgroundMode } from '@ionic-native/background-mode';
/**
 * Generated class for the TimeCountPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-time-count',
  templateUrl: 'time-count.html',
})
export class TimeCountPage {
  @ViewChild(TimerComponent) timer: TimerComponent;
  timeInSeconds: number;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public alertCtrl: AlertController,
              private backgroundMode: BackgroundMode) {
    this.timeInSeconds = navParams.get('timer');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TimeCountPage');
    this.backgroundMode.enable();
  }
  ionViewDidLeave() {
    console.log('ionViewDidLeave TimeCountPage');
    this.backgroundMode.disable();
  }
  close() {
    if(this.timer.hasFinished()) {
      this.navCtrl.pop();
    } else {
      this.showConfirm();
    }
  }

  ngOnInit() {
    setTimeout(() => {
      this.timer.startTimer();
    }, 1000)
  }

  showConfirm() {
    let confirm = this.alertCtrl.create({
      title: '',
      message: '是否退出計時？',
      buttons: [
        {
          text: '否（NO）',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: '是（YES)',
          handler: () => {
            console.log('Agree clicked');
            this.timer.pauseTimer();
            this.navCtrl.pop();
          }
        }
      ]
    });
    confirm.present();
  }
}
