import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Events } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { ChartPage } from '../chart/chart';
import { SendMailPage } from '../send-mail/send-mail';
import moment from 'moment';

/**
 * Generated class for the ListDetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-list-detail',
  templateUrl: 'list-detail.html',
})
export class ListDetailPage {
  detailItem: any;
  startDate: string;
  finishDate: string;
  lastCountDate: string;
  tmpDetailItem: any;
  data: any[];
  today = new Date().toISOString();

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public dataService: DataProvider, 
              public events: Events,
              public alertCtrl: AlertController) {
    this.detailItem = navParams.get('detailItem');
    this.tmpDetailItem = this.detailItem;
  }
  ngOnInit() {
    this.dataService.getData().then((data) => { 
      if(data){
        this.data = JSON.parse(data) || [];
        console.log("Parse Json " + this.data);
      } 
    });
  }

  getTotalCount() {
    return this.detailItem.complete + parseInt(this.detailItem.history_count);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListDetailPage');
  }

  goChartPage() {
    this.navCtrl.push(ChartPage, {
      'detailItem': this.detailItem 
    });
  }
  getPropotion(complete: number, target_count: number) {
    return (complete%target_count==0&&complete>0)?"100%":Math.round((complete%target_count)/target_count *100) + "%";
  }

  getStartDate() { // using moment.js
    var date = new Date(this.detailItem.created);
    var formattedDate = moment(date).format('DD/MM/YYYY');
    return formattedDate;
  }
  getFinishDate() {
    var date = new Date(this.detailItem.endDate);
    var formattedDate = moment(date).format('DD/MM/YYYY');
    return formattedDate;    
  }
  getLastCoundDate() {
    var date = new Date(this.detailItem.updated);
    var formattedDate = moment(date).format('DD/MM/YYYY');
    return formattedDate;
  }

  isAppCategory() {
    if(this.detailItem.category_type == "app_category")
      return true;
    return false;
  }

  showAlert(message: string) {
    let alert = this.alertCtrl.create({
      title: '',
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

  close() {
    this.navCtrl.pop();
  }

  onDone() {
    if(this.tmpDetailItem.target_count<0 || this.tmpDetailItem.history_count<0 ||this.tmpDetailItem.complete<0 ) {
      this.showAlert("請選擇一個正數");
      return;
    }
    const index = this.data.findIndex(item => item.categoryId === this.detailItem.categoryId);
    if(index != -1) {
      console.log("Index", index);
      this.data[index] = this.tmpDetailItem;

      this.dataService.save(this.data);
    }
    this.detailItem = this.tmpDetailItem;

    console.log("detail Item", this.data);
    this.events.publish('reloadHomePage');
    this.navCtrl.popToRoot();
  }

  openMailPage() {
    this.navCtrl.push(SendMailPage, {
      page: "detail",
      detailItem: this.detailItem
    });
  }

}
