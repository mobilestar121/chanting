import { Component, Renderer } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, AlertController } from 'ionic-angular';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { TimeCountPage } from '../time-count/time-count';

/**
 * Generated class for the TimeModalPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-time-modal',
  templateUrl: 'time-modal.html',
})
export class TimeModalPage {
  times:any[];
  notifyTime: any;
  notifications: any[] = [];
  chosenHours: number;
  chosenMinutes: number;

  defaultTime: number;

  selectedCount: number;

  simpleColumns: any[];

  constructor(public navCtrl: NavController, 
              public platform: Platform, 
              public navParams: NavParams, 
              public renderer: Renderer, 
              // public viewCtrl: ViewController, 
              public alertCtrl: AlertController, 
              public localNotifications: LocalNotifications) {
      
      var colOptions1 = [];
      var colOptions2 = [];
      for(var i=0;i<12;i++) {
        var item = {text: String(i)+'小时', value: String(i)}
        colOptions1.push(item);
      }
      for(var i=0;i<60;i++) {
        var item2 = {text: String(i)+'分钟', value: String(i)}
        colOptions2.push(item2);
      }
      this.simpleColumns = [
      {
        name: 'col1',
        options: colOptions1
      },{
        name: 'col2',
        options: colOptions2
      }
    ];
      
      this.chosenHours = 0;
      this.chosenMinutes = 1;
      let customTime = new Date();
      customTime.setHours(this.chosenHours);
      customTime.setMinutes(this.chosenMinutes);
      // this.notifyTime = moment(customTime).format();
      this.notifyTime = '0 1';      
 
      // this.chosenHours = new Date().getHours();
      // this.chosenMinutes = new Date().getMinutes();
      // this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'my-popup', true);
      
      this.times = [{title:"5分钟", min:5, checked: false},
        {title:"10分钟", min:10, checked: false},
        {title:"15分钟", min:15, checked: false},
        {title:"30分钟", min:30, checked: false},
        {title:"40分钟", min:40, checked: false},
        {title:"1小时", min:60, checked: false}];

    this.localNotifications.on("click", (notification, state) => {
        let alert = this.alertCtrl.create({
            title: "Notification Clicked",
            subTitle: "You just clicked the scheduled notification",
            buttons: ["OK"]
        });
        alert.present();
    });
  }

  setDefaultTime() {
    if(this.defaultTime == undefined)       
      return;
    this.selectedCount = this.defaultTime;    
    // schedule local notification
    // this.schedule(this.defaultTime);
    // this.showAlert('Default Time Set');
    this.gotoTimePage();
  }
  setManualTime() {
    console.log("setManualTime", this.chosenHours, this.chosenMinutes);
    let minutes = this.chosenHours*60 + this.chosenMinutes;
    this.selectedCount = minutes;
    // schedule local notification
    // this.schedule(minutes);
    // this.showAlert('Manual Time Set');
    this.gotoTimePage();
  }

  showAlert(message: string) {
    let alert = this.alertCtrl.create({
        title: "",
        subTitle: message,
        buttons: ["OK"]
    });
    alert.present();
  }

  timeRadioChange(ev) {
    console.log("timeRadioChange", ev);
    
    if(ev == undefined) {       
      this.defaultTime = undefined;
      return;
    }
    this.defaultTime = parseInt(ev);
    
  }

  timePickerChange(time){
    console.log("timePickerChange---", time.col1.value);
    console.log("notifyTime",this.notifyTime);
    this.chosenHours = time.col1.value;
    this.chosenMinutes = time.col2.value;
  }
  
  // schedule local notification
  public schedule(minutes: number) {
    let firstNotificationTime = new Date();
    firstNotificationTime.setHours(firstNotificationTime.getHours() + (firstNotificationTime.getMinutes() + minutes)/60);
	  firstNotificationTime.setMinutes((firstNotificationTime.getMinutes() + minutes)%60);
 
    console.log("firstNotificationTime", firstNotificationTime);

    let notification = {
        title: 'Chanting!',
        text: 'Chanting Notification :)',
        sound: 'file://assets/audio/meditation.mp3',
        at: firstNotificationTime,
        every: 'week'
    };

    this.notifications.push(notification);

    if(this.platform.is('cordova')){
        console.log("Notifications to be scheduled: ", this.notifications);
        // Cancel any existing notifications
        this.localNotifications.cancelAll().then(() => {
            console.log("scheduled: ", this.notifications);
            // Schedule the new notifications
            this.localNotifications.schedule(this.notifications);
 
            this.notifications = [];
 
            let alert = this.alertCtrl.create({
                title: 'Notifications set',
                buttons: ['Ok']
            });
 
            alert.present(); 
        }); 
    }
  }
 

  cancelAll(){
 
    this.localNotifications.cancelAll();
 
    let alert = this.alertCtrl.create({
        title: 'Notifications cancelled',
        buttons: ['Ok']
    });
 
    alert.present();
 
  }

  close() {
    // this.viewCtrl.dismiss();
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TimeModalPage');
  }

  gotoTimePage() {
    this.navCtrl.push(TimeCountPage, {
        timer: this.selectedCount * 60
      });
  }

}
