import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TimeModalPage } from './time-modal';

@NgModule({
  declarations: [
    TimeModalPage,
  ],
  imports: [
    IonicPageModule.forChild(TimeModalPage),
  ],
  exports: [
    TimeModalPage
  ]
})
export class TimeModalPageModule {}
