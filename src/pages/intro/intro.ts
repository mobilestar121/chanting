import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';

/**
 * Generated class for the IntroPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html',
})
export class IntroPage {
  slides = [
    {

      image: "assets/img/1.jpg",
    },
    {

      image: "assets/img/2.jpg",
    },
    {

      image: "assets/img/3.jpg",
    },
    {
      image: "assets/img/4.jpg",

    }
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IntroPage');
  }

  openPage() {
    this.navCtrl.setRoot(HomePage);
  }

}
