import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the ChantingServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ChantingServiceProvider {
  baseUrl: String;
  data: any;

  constructor(public http: Http) {

    this.baseUrl = 'http://truebuddhaschool.com/videowebserv/mantra.php';
  }
  getCategories() {
    return this.http.get(this.baseUrl + '?op=viewMantraCategoryApp')
      .map(res => res.json());
  }
  getSubCategories(subCategoryId) {
    return this.http.get(this.baseUrl + '?op=viewMantraCategoryApp&id='+subCategoryId)
      .map(res => res.json());
  }
  getItemsInCategory(categoryId) {
    return this.http.get(this.baseUrl + '?catid=' + categoryId)
      .map(res => res.json());
  }

  postEmail(sender_name, sender_email, content, content_type) {
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    let postParams = {
      op: 'send_information',
      sender_name: sender_name,
      sender_email: sender_email,
      content:content,
      content_type:content_type
    }

    console.log("postParams", postParams);

    return this.http.post("http://truebuddhaschool.com/videowebserv/chanting.php", postParams, options);

  }
  getMail(sender_name, sender_email, content, content_type){
    return this.http.get("http://truebuddhaschool.com/videowebserv/chanting.php?op=send_information&sender_name="+sender_name
            +"&sender_email="+sender_email+"&content="+content+"&content_type="+content_type)
      .map(res => res.json());
  }

  postRequest() {
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    let postParams = {
      title: 'foo',
      body: 'bar',
      userId: 1
    }

    this.http.post("http://jsonplaceholder.typicode.com/posts", postParams, options)
      .subscribe(data => {
        console.log(data['_body']);

      }, error => {
        console.log(error);
      });
  }

  load(category, limit) {
    if (this.data) {
      return Promise.resolve(this.data);
    }

    return new Promise (resolve => {
      this.http.get(this.baseUrl + '/' + category + '/top.json?limit=' + limit)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    })
  }

}
