import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

/*
  Generated class for the DataProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class DataProvider {

  constructor(public storage: Storage) {
    console.log('Hello DataProvider Provider');
  }
  //when button click
  getButtonClickSoundOnOffStatus() {
    return this.storage.get('clickStatus');
  }
  setButtonClickSoundOnOffStatus(status: boolean) {
    this.storage.set('clickStatus', status);
  }
  // when chanting count reaches target count
  getAlarmStatus() {
    return this.storage.get('alarmStatus');
  }
  setAlarmStatus(status: boolean) {
    this.storage.set('alarmStatus', status);
  }

  // chanting list data
  getData() {
    return this.storage.get('chantingData');  
  }
 
  save(data){
    let newData = JSON.stringify(data);
    this.storage.set('chantingData', newData);
  }
  clearKeys() {
    this.storage.clear().then(() => {
      console.log('Keys have been cleared');
    });
  }

  //save category names for listing chanting items
  setCategoriesList(list) {
    let newData = JSON.stringify(list);
    this.storage.set('categoriesList', newData);
  }
  getCategoriesList() {
    return this.storage.get('categoriesList');  
  }

  setUserData(data) {
    let newData = JSON.stringify(data);
    this.storage.set('chantingUserData', newData);
  }
  getUserData() {
    return this.storage.get('chantingUserData');
  }
}
