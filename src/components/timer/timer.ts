import { Component, Input } from '@angular/core';
import { ITimer } from './itimer';
import { SmartAudioProvider } from '../../providers/smart-audio/smart-audio';
import { DataProvider } from '../../providers/data/data';

/**
 * Generated class for the TimerComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'timer',
  templateUrl: 'timer.html'
})
export class TimerComponent {

  @Input() timeInSeconds: number;
    public timer: ITimer;
    sixthDigit: string;
    fifthDigit: string;
    forthDigit: string;
    thirdDigit: string;
    secondDigit: string;
    firstDigit: string;

    constructor(public smartAudio: SmartAudioProvider, 
                public dataService: DataProvider,) {
    }

    ngOnInit() {
        this.initTimer();
    }

    hasFinished() {
        return this.timer.hasFinished;
    }

    initTimer() {
        if(!this.timeInSeconds) { this.timeInSeconds = 0; }

        this.timer = <ITimer>{
            seconds: this.timeInSeconds,
            runTimer: false,
            hasStarted: false,
            hasFinished: false,
            secondsRemaining: this.timeInSeconds
        };

        // this.timer.displayTime = this.getSecondsAsDigitalClock(this.timer.secondsRemaining);
        this.getSecondsAsDigitalClock(this.timer.secondsRemaining);
    }

    startTimer() {
        this.timer.hasStarted = true;
        this.timer.runTimer = true;
        this.timerTick();
    }

    pauseTimer() {
        this.timer.runTimer = false;
    }

    resumeTimer() {
        this.startTimer();
    }

    timerTick() {
        setTimeout(() => {
            
            if (!this.timer.runTimer) { return; }
            console.log("timer still running");
            this.timer.secondsRemaining--;
            // this.timer.displayTime = this.getSecondsAsDigitalClock(this.timer.secondsRemaining);
            this.getSecondsAsDigitalClock(this.timer.secondsRemaining);
            if (this.timer.secondsRemaining > 0) {
                this.timerTick();
            }
            else {
                this.timer.hasFinished = true;
                console.log("timer finished");
                this.dataService.getAlarmStatus().then((status) => {
                    if(status) {
                        this.smartAudio.play('meditation'); 
                    }
                });
                 
            }
        }, 1000);
    }

    getSecondsAsDigitalClock(inputSeconds: number) {
        var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        var hoursString = '';
        var minutesString = '';
        var secondsString = '';
        hoursString = (hours < 10) ? "0" + hours : hours.toString();
        minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
        secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();

        this.sixthDigit = hoursString.substr(0,1);
        this.fifthDigit = hoursString.substr(1,1);
        this.forthDigit = minutesString.substr(0,1);
        this.thirdDigit = minutesString.substr(1,1);
        this.secondDigit = secondsString.substr(0,1);
        this.firstDigit = secondsString.substr(1,1);
        // return hoursString + ':' + minutesString + ':' + secondsString;
    }

}
